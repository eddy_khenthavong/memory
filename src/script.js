$(document).ready(function () {

  /*-----------------Initialisation--------------------------*/

  let Scores = {
    scoreJoueur: 0,
    scoreOrdi: 0,
    HistoJoueur: [],
    HistoOrdi: [],
  };
  let nbTuilesMax = 32;
  let valeursPossibleTuiles = [];
  let dontClick = Math.floor(
    Math.random() * (valeursPossibleTuiles.length * 2)
  );
  let dontClick2 = Math.floor(
    Math.random() * (valeursPossibleTuiles.length * 2)
  );
  for (i = 1; i <= nbTuilesMax / 2; i++) {  // Initialisation du nombre de tuiles et des valeurs des tuiles possibles
    valeursPossibleTuiles.push(i);
  }

  /*-----------------Création des tuiles et Assignation des valeurs-----------------------------*/

//ICI RECUPERER LES DONNEES DE PHP POUR L'AFFICHAGE DES TUILES

  while ($(`input`).length < nbTuilesMax) {  //Tant que le nombre d'enfants de #container < nombre de tuiles
    
    $("#container").append($("<div/>", { class: "tile" }).append($("<input/>", { type: "hidden", value: "" }))); // créer une tuile et assigner une valeur random
  }

  /*-----------------Click handling-----------------------------*/

  let valeurActuelle =
    valeursPossibleTuiles[
    Math.floor(Math.random() * valeursPossibleTuiles.length)
    ];

  $("#container").on("click", ".tile", function () {
    while ($(this).children("input").attr("value").length == 0) {

      if ($(`input[value="${valeurActuelle}"]`).length < 2) {    //Si il n'y a pas déjà 2 tuiles avec la même valeur
        
        $(this).children("input").attr("value", valeurActuelle);
      }
      valeurActuelle =
        valeursPossibleTuiles[
        Math.floor(Math.random() * valeursPossibleTuiles.length)
        ];
    }
    $(this).addClass("animation");

    if ($(this).children().length < 2) {      // Donner une valeur à la div cliquée si il y'en a moins de 2 

      $(this).append(`<p>${$(this).children("input").attr("value")}</p>`);
    }
    if ($(".animation").length === 2) { //Si 2 divs ont la classe anim et valeur des deux divs égales -> Supprimer les divs -> envoyer les données à PHP

//ICI FAIRE PASSER LE TOUR EN PHP (JOUEUR/ORDI)


      $("h1").toggleClass("ordi");  // Donne une classe 'Ordi' au h1 pour track le tour de l'ordinateur

/*------------------------------------Tour de l'ordinateur-------------------------------------*/
      if ($(".ordi").length === 1) {

        setTimeout(() => {   // Après 3 secondes, sélectionne une div aléatoire et simule le Click
          dontClick = Math.floor(
            Math.random() * (valeursPossibleTuiles.length * 2)
          );
          if ($('.tile').length > 0) {
            while (dontClick > valeursPossibleTuiles.length * 2 - 1) {
              dontClick = Math.floor(
                Math.random() * (valeursPossibleTuiles.length * 2)
              );
            }
          }
          $(`.tile:eq(${dontClick})`).trigger("click"); // Click 1 de l'ordinateur
        }, 3000);

        setTimeout(() => { // Après 3.5 secondes, sélectionne une div aléatoire différente de la première et simule le Click
          dontClick2 = Math.floor(
            Math.random() * (valeursPossibleTuiles.length * 2)
          );
          if ($('.tile').length > 0) {
            while (
              dontClick === dontClick2 ||
              dontClick2 > valeursPossibleTuiles.length * 2 - 1
            ) {
              dontClick2 = Math.floor(
                Math.random() * (valeursPossibleTuiles.length * 2)
              );
            }
          }
          $(`.tile:eq(${dontClick2})`).trigger("click"); // Click 2 de l'ordinateur
        }, 3500);
      }

/*-------------------------------------------------------------------------*/


      if (
        $(".animation:eq(0)").children().val() === $(".animation:eq(1)").children().val()) { // Si les deux tuiles ont la même valeur

//ICI ENVOYER LE GAGNANT DU TOUR A PHP 
//STOCKER L'HISTORIQUE EN PHP
//INCREMENTER LES SCORES EN PHP

        $("div").prop("disabled", true); // Désactive le 'on-click' après 2 click sur une divs
        if ($(".ordi").length == 1) {   // Gestion du score
          Scores.scoreJoueur++;
          Scores.HistoJoueur.push($(".animation:eq(0)").children().val());
        } else {
          Scores.scoreOrdi++;
          Scores.HistoOrdi.push($(".animation:eq(0)").children().val());
        }
        
        setTimeout(() => {    // Au bout d'1 seconde, supprime les divs, réactive le 'on-click' et active l'animation flip-reverse pour les prochains clicks.
          $(".animation").remove();
          $(".tile").addClass("animation-reverse");
          $("div").prop("disabled", false);
        }, 1000);


        setTimeout(() => {
          $(".tile").removeClass("animation-reverse");


//ICI RECUPERER LES DONNEES FINALES DE PHP POUR LES AFFICHER

          if ($(".tile").length === 0) {      // Affichage des résultats en fin de partie
            $("#container").css('flex-direction', 'column')
            $("#container").append(`<h2>Score joueur : ${Scores.scoreJoueur} <br />Score Ordi : ${Scores.scoreOrdi}</h2>`);
            $("#container").append(`<ul><li>Historique Joueur: ${Scores.HistoJoueur}</li></ul>`);
            $("#container").append(`<ul><li>Historique IA: ${Scores.HistoOrdi}</li></ul>`);

          }
        }, 1500);

        let lala = parseInt($(".animation:eq(0)").children().val());
        valeursPossibleTuiles.splice(valeursPossibleTuiles.indexOf(lala), 1); // Suppression de la paire trouvée des paires possibles
        valeurActuelle =
          valeursPossibleTuiles[
          Math.floor(Math.random() * valeursPossibleTuiles.length)
          ];

        $("input").attr("value", ""); // Réinitialise les valeurs des tuiles à 0 à chaque fois qu'une paire est trouvée

      } else {
        $("div").prop("disabled", true); // Désactive le 'on-click' après 2 click sur une divs
        setTimeout(function () {  // Au bout d'1 seconde, ré-active le 'on-click', désactive l'animation flip, cache les valeurs et active l'animation reverse.
          $("div").removeClass("animation");
          $("p").remove();
          $(".tile").addClass("animation-reverse");
          $("div").prop("disabled", false);
        }, 1000);
        setTimeout(() => {  // Au bout de 2 secondes, active l'animation flip-reverse pour les prochains clicks.
          $(".tile").removeClass("animation-reverse");
        }, 2000);
      }
    }
  });
});