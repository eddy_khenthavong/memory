<?php
/*
Donne le nombre de tuiles à afficher
Compte les points de chaque joueurs
Se souvient de l'historique des paires tirées pour chaque joueur
*/

/*
1/ Accueil -> demande le nom du joueur et son mail
2/ Partie
3/ Affichage du gagnant
4/ Envoie par mail le récap de la partie
*/
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="src/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script type="text/javascript" src="src/script.js"></script>
    <title>Memory</title>
</head>

<body>
    <div id="title">
        <h1>Memory</h1>
    </div>
    <div id="container">

    </div>
</body>

</html>